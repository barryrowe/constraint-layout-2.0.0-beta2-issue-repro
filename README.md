**Update (2020-06-01)** This appears to be fixed in v2.0.0-beta6. 
[View Google Issue](https://issuetracker.google.com/issues/145736492)

On Android 6 (API 23) constraint layout v2.0.0-beta2 fails to properly expand 
some layouts which use 0dp to match constraints. There appears to be a buffer
at the bottom (that looks equivalent to the soft navigation bar) that views will
not expand into. This does not happen with stable (1.1.3).

| Stable (v1.1.3) | Bugged (v2.0.0-beta2) |
| ------ | ------ |
| ![working](./screenshots/stable-1.1.3.png) | ![bugged](./screenshots/bugged-v2.0.0-beta2.png) |
